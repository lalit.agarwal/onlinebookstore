package com.dev.bootcamp.onlinebookstore;

import com.dev.bootcamp.onlinebookstore.entity.Book;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BookStoreController {

    @GetMapping (value = "/application")
    public Book getBookTitle() {
        Book book = new Book("Book Title1");
        return book;
    }
}
