package com.dev.bootcamp.onlinebookstore.entity;

public class Book {

    private final String title;

    public Book (String title) {
        this.title = title;
    }

    public String getTitle() {
        return this.title;
    }
}
