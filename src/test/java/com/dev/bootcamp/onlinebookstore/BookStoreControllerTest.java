package com.dev.bootcamp.onlinebookstore;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class BookStoreControllerTest {

    @Test
    public void shouldReturnControllerInstance() {
        BookStoreController bookStoreController = new BookStoreController();
        assertNotNull(bookStoreController);
    }

    @Test
    public void shouldGetMethodReturnString() {
        BookStoreController bookStoreController = new BookStoreController();
        assertEquals("Book Title1", bookStoreController.getBookTitle().getTitle());
    }
}
