package com.dev.bootcamp.onlinebookstore;

import com.dev.bootcamp.onlinebookstore.entity.Book;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class BookTest {

    @Test
    public void shouldReturnBookInstance() {
        Book book = new Book("Book Title1");
        assertNotNull(book);
    }

    @Test
    public void shouldReturnTheTitle() {
        Book book = new Book("Book Title1");
        assertEquals("Book Title1", book.getTitle());
    }
}
